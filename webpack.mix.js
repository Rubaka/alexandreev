let mix = require('laravel-mix')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
  .js('resources/assets/js/category.js', 'public/js')
  .sass('resources/assets/sass/app.sass', 'public/css').version()

mix.js('resources/assets/admin/js/app.js', 'public/js/admin')
  .js('resources/assets/admin/js/category.js', 'public/js/admin')
  .js('resources/assets/admin/js/contacts.js', 'public/js/admin')
  .js('resources/assets/admin/js/content_items.js', 'public/js/admin')
  .js('resources/assets/admin/js/content_items_index.js', 'public/js/admin')
  .sass('resources/assets/admin/sass/app.sass', 'public/css/admin').version()
