<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',  'SiteController@index');
Route::get('/contacts',  'SiteController@contacts');
Route::get('/category/{id}',  'SiteController@category');

Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');

//Если нужно разрешить регистрацию других пользователей, тогда нужно ввести флаг для роли админа
Route::match(['get', 'post'], 'register', function() {
   return redirect('/');
});

Route::prefix('admin')->middleware('auth')->group(function() {
    Route::get('/home-page', 'Admin\IndexController@index');
    Route::post('/home-page/save', 'Admin\IndexController@update');

    Route::get('/category', 'Admin\CategoryController@index');
    Route::post('/category/save', 'Admin\CategoryController@save');
    Route::post('/category/upload-image', 'Admin\CategoryController@uploadImage');

    Route::resource('content-item', 'Admin\ContentItemController');
    Route::post('/content-item/upload-image', 'Admin\ContentItemController@uploadImage');
    Route::get('/content-item/{id}/delete', 'Admin\ContentItemController@destroy');

    Route::get('/contacts', 'Admin\ContactsController@index');
    Route::post('/contacts/save', 'Admin\ContactsController@update');
});