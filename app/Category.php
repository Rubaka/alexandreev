<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    public function contentItems()
    {
        return $this->hasMany(ContentItem::class);
    }

    public function listData()
    {
        $parentCategories = $this->whereNull('parent_category_id')
            ->orderBy('weight')
            ->get()
            ->toArray();

        $childCategories = $this->whereNotNull('parent_category_id')
            ->orderBy('weight')
            ->get()
            ->groupBy('parent_category_id')
            ->toArray();

        foreach ($parentCategories as &$parentCategory) {
            $parentId = $parentCategory['id'];
            $parentCategory['children'] = $childCategories[$parentId] ?? [];
            $parentCategory['action'] = false;

            if ($parentCategory['image']) {
                $parentCategory['image'] = [
                    'action' => 'old',
                    'name' => $parentCategory['image']
                ];
            }

            foreach ($parentCategory['children'] as &$childCategory) {
                $childCategory['action'] = false;
                if ($childCategory['image']) {
                    $childCategory['image'] = [
                        'action' => 'old',
                        'name' => $childCategory['image']
                    ];
                }
            }
        }

        return $parentCategories;
    }

    public function saveListData($categories)
    {
        foreach ($categories as $keyCategory => $category) {
            $categoryId = $this->saveCategory($category, $keyCategory);

            if (!empty($category['children']) && $category['action'] != 'delete') {
                foreach ($category['children'] as $keyChild => $childCategory) {
                    $this->saveCategory($childCategory, $keyChild, $categoryId);
                }
            }
        }

        return true;
    }

    public function saveCategory($category, $weight, $parentCategoryId = null)
    {
        $action = $category['action'];
        $categoryId = $category['id'] ?? null;
        $storage = \Storage::disk('public');

        if (!$categoryId) {
            $modelCategory = new Category();
        } else {
            $modelCategory = $this->find($categoryId);
        }

        $modelCategory->name = $category['name'];
        $modelCategory->weight = $weight;
        $modelCategory->show_on_main = $category['show_on_main'];
        $modelCategory->weight_in_bar = $category['weight_in_bar'];
        $modelCategory->parent_category_id = $parentCategoryId;
        $modelCategory->save();
        $categoryId = $modelCategory->id;

        if (!empty($category['image']) && $category['image']) {
            $image = $category['image']['name'];
            $path = 'images/categories/' . $image;
            $pathTmp = 'images/tmp/' . $image;
            $actionImage = $category['image']['action'];

            if ($actionImage == 'new') {
                $storage->move($pathTmp, $path);
            }

            if ($actionImage == 'delete') {
                $storage->delete([$pathTmp, $path]);
                $image = null;
            }

            if ($actionImage == 'new' || 'delete') {
                $modelCategory = $this->find($categoryId);
                $modelCategory->image = $image;
                $modelCategory->save();
            }
        } else {
            //Если нет изображения, тогда сбрасываем чекбокс показа на главной
            $modelCategory = $this->find($categoryId);
            $modelCategory->show_on_main = false;
            $modelCategory->save();
        }

        if ($action == 'delete') {
            $modelCategory = $this->find($categoryId);
            if ($modelCategory->image) {
                $storage->delete('images/categories/' . $modelCategory->image);
            }

            $modelCategory->delete();
        }

        return $categoryId;
    }

    public static function getMenu()
    {
        $itemsBar = [];
        $itemsMenu = self::select('name', 'id', 'show_on_main', 'image', 'weight_in_bar')
            ->whereNull('parent_category_id')
            ->orderBy('weight')
            ->get();

        $itemsMenu->transform(function ($item) use (&$itemsBar) {
            $childCategory = Category::select('name', 'id', 'show_on_main', 'image', 'weight_in_bar')
                ->where('parent_category_id', $item->id)
                ->orderBy('weight')
                ->get();

            if ($item->show_on_main) {
                $itemsBar[] = $item->toArray();
            }

            $itemsBar = array_merge($itemsBar, $childCategory->where('show_on_main', 1)->toArray());
            $item->children = $childCategory;
            return $item;
        });

        return [
            'itemsMenu' => $itemsMenu,
            'itemsBar' => collect($itemsBar)->sortBy('weight_in_bar')->toArray(),
        ];

    }
}
