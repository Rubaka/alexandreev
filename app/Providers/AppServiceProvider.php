<?php

namespace App\Providers;

use App\Category;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (!in_array('admin', explode('/', request()->getPathInfo())) && !app()->runningInConsole()) {
            $menu = Category::getMenu();
            \View::share('itemsMenu', $menu['itemsMenu']);
            \View::share('itemsBar', $menu['itemsBar']);
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
