<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install site';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $pathImages = storage_path('app/public/images/');
        \File::makeDirectory($pathImages . 'tmp', 0775, true, true);
        \File::makeDirectory($pathImages . 'categories', 0775, true, true);
        $this->info('Site installed');
    }
}
