<?php

namespace App\Http\Controllers;
use App\Category;
use App\Page;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function index()
    {
        $page = Page::byAlias('index');

        return view('index', [
            'page' => $page
        ]);
    }

    public function category($id)
    {
        return view('category', ['category' => Category::find($id)]);
    }

    public function contacts()
    {
        $page = Page::byAlias('contacts');
        $data = json_decode($page->data, true);

        return view('contacts', [
            'page' => $page,
            'image' => $image ?? null,
            'phone' => $data['phone'] ?? null,
            'email' => $data['email'] ?? null,
            'links' => $data['links'] ?? null,
        ]);
    }
}
