<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index()
    {
        return view('admin.category', [
            'categories' => json_encode((new Category())->listData())
        ]);
    }

    public function uploadImage(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|mimes:jpeg,jpg|dimensions:min_width=300,min_height=300'
        ]);

        $fileImage = $request->file('image');
        $fileName = $fileImage->hashName();
        $imagePath = storage_path('app/public/images/tmp/' . $fileName);

        $image = \Image::make($fileImage);

        $image->resize(500, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        $image->save($imagePath);

        return $fileName;
    }

    public function save(Request $request)
    {
        $saveResult = (new Category())
            ->saveListData($request->get('categories'));

        if ($saveResult) {
            return [
                'msg' => 'ok',
                'categories' => (new Category())->listData()
            ];
        }

        return ['msg' => 'error'];
    }
}
