<?php

namespace App\Http\Controllers\Admin;

use App\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index()
    {
        $page = Page::byAlias('index');
        $storage = \Storage::disk('public');
        $filePath = 'images/index/header.jpg';

        if ($storage->exists($filePath)) {
            $image = $storage->url($filePath);
        }

        return view('admin.index', ['page' => $page, 'image' => $image ?? null]);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'content' => 'required',
            'image' => 'mimes:jpeg,jpg|dimensions:min_width=1000,min_height=1000'
        ]);

        if ($request->has('image')) {
            $image = \Image::make($request->file('image'));
            $imagePath = storage_path('app/public/images/index/header.jpg');

            $image->resize(null, 1000, function ($constraint) {
                $constraint->aspectRatio();
            });

            $image->save($imagePath);
        }

        $page = Page::byAlias('index');
        $page->content = $request->get('content');
        $page->save();

        return back()->with('message', 'Page updated!');
    }
}
