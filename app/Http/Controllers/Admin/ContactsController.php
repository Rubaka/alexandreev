<?php

namespace App\Http\Controllers\Admin;

use App\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactsController extends Controller
{
    public function index()
    {
        $page = Page::byAlias('contacts');
        $storage = \Storage::disk('public');
        $filePath = 'images/avatar.jpg';

        if ($storage->exists($filePath)) {
            $image = $storage->url($filePath);
        }

        $data = json_decode($page->data, true);

        return view('admin.contacts', [
            'page' => $page,
            'image' => $image ?? null,
            'phone' => $data['phone'] ?? null,
            'email' => $data['email'] ?? null,
            'links' => json_encode($data['links'] ?? []),
        ]);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'content' => 'required',
            'image' => 'mimes:jpeg,jpg|dimensions:min_width=300,min_height=300'
        ]);

        if ($request->has('image')) {
            $image = \Image::make($request->file('image'));
            $imagePath = storage_path('app/public/images/avatar.jpg');

            $image->resize(null, 1000, function ($constraint) {
                $constraint->aspectRatio();
            });

            $image->save($imagePath);
        }

        $page = Page::byAlias('contacts');
        $page->content = $request->get('content');
        $page->data = json_encode(
            [
                'phone' => $request->get('phone'),
                'email' => $request->get('email'),
                'links' => json_decode($request->get('links'))
            ]
        );

        $page->save();

        return back()->with('message', 'Page updated!');
    }
}
