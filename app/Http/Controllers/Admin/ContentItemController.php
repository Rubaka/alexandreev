<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\ContentItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContentItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = ContentItem::orderBy('weight');
        if (\request()->has('category_id')) {
            $query->where('category_id', \request()->get('category_id'));
        }

        return view('admin.content_item.index',
            [
                'items' => $query->get(),
                'categories' => Category::getMenu()['itemsMenu'],
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.content_item.create', [
            'item' => new ContentItem(),
            'categories' => Category::getMenu()['itemsMenu'],
            'images' => []
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'category' => 'required',
            'weight' => 'numeric|max:100'
        ]);

        $contentItem = new ContentItem();
        $contentItem->title = $request->get('title');
        $contentItem->name = $request->get('name');
        $requestCategories = collect($request->get('category'))->filter()->toArray();
        $contentItem->category_id = end($requestCategories);
        $contentItem->body = $request->get('body');
        $contentItem->background = $request->get('background');
        $contentItem->weight = $request->get('weight');
        $contentItem->save();

        self::storeImage($contentItem->id, $request->get('images'));

        return redirect('/admin/content-item')->with('message', 'Item created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = ContentItem::find($id);

        return view('admin.content_item.edit',
            [
                'item' => ContentItem::find($id),
                'categories' => Category::getMenu()['itemsMenu'],
                'images' => self::templateDataImages($item->images)
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'category' => 'required',
            'weight' => 'numeric|max:100'
        ]);

        $contentItem = ContentItem::find($id);
        $contentItem->title = $request->get('title');
        $contentItem->name = $request->get('name');
        $contentItem->body = $request->get('body');
        $contentItem->background = $request->get('background');
        $requestCategories = collect($request->get('category'))->filter()->toArray();
        $contentItem->category_id = end($requestCategories);
        $contentItem->weight = $request->get('weight');
        $contentItem->save();

        self::storeImage($contentItem->id, $request->get('images'));

        return redirect('/admin/content-item')->with('message', 'Item updated!');
    }

    public function destroy($id)
    {
        ContentItem::find($id)->delete();
        return back()->with('message', 'Item deleted!');
    }

    public function uploadImage(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|mimes:jpeg,jpg|dimensions:min_width=300,min_height=300'
        ]);

        $fileImage = $request->file('image');
        $fileName = $fileImage->hashName();
        $imagePath = storage_path('app/public/images/tmp/' . $fileName);

        $image = \Image::make($fileImage);

        $image->resize(null, 900, function ($constraint) {
            $constraint->aspectRatio();
        });

        $image->save($imagePath);

        return $fileName;
    }

    public static function storeImage($contentItemId, $images)
    {
        $images = json_decode($images, true);

        foreach ($images as $key => $image) {
            $name = $image['name'];

            if ($image['action'] == 'delete') {
                \DB::table('content_item_images')
                    ->where('content_item_id', $contentItemId)
                    ->where('name', $name)
                    ->delete();

                \Storage::disk('public')
                    ->delete('images/tmp/' . $name, 'images/content_item/' . $name);
            }

            if ($image['action'] == 'new') {
                \Storage::disk('public')
                    ->move('images/tmp/' . $name, 'images/content_item/' . $name);

                \DB::table('content_item_images')
                    ->insert(
                        [
                            'content_item_id' => $contentItemId,
                            'name' => $name,
                            'weight' => $key
                        ]
                    );
            }
        }
    }

    public static function templateDataImages($dataImages)
    {
        $images = [];
        foreach ($dataImages as $image) {
            $images[] = [
                'name' => $image->name,
                'action' => 'old'
            ];
        }

        return $images;
    }
}
