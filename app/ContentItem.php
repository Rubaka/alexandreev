<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentItem extends Model
{
    public function images()
    {
        return $this->hasMany(ContentItemImage::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
