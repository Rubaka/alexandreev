<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('alias');
            $table->text('content');
            $table->timestamps();
        });

        \DB::table('pages')->insert(['content' => 'Hello!', 'alias' => 'index']);
        \DB::table('pages')->insert(['content' => 'Hello! Contacts!', 'alias' => 'contacts']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
