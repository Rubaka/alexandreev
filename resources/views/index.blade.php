@extends('template')

@section('content')
    <div
        class="uk-background-cover uk-position-relative"
        style="background-image: url('/storage/images/index/header.jpg?{{ filemtime(storage_path('app/public/images/index/header.jpg')) }}');"
        uk-height-viewport="offset-top: true"
    >
        <a class="uk-position-bottom-center" href="#title" uk-scroll="offset: 100;">
            <img src="/images/template/to_bottom.png" class="uk-margin-bottom">
        </a>
    </div>

    <div class="uk-container-expand uk-padding uk-text-center index_title" id="title">
        <p>{{ $page->content }}</p>
    </div>

    @include('category_bar')
@endsection