@extends('template')

@section('content')
@foreach($category->contentItems as $item)
    @if($item->images->count() == 1)
    <div class="job_container">
        <div class="job" @if($item->background)style="background-color: {{$item->background}}"@endif>
            <img src="/storage/images/content_item/{{$item->images[0]->name}}" alt="">
        </div>

        <div class="uk-container uk-container-expand">
            <div class="uk-margin-top uk-margin-bottom">
                <div class="uk-grid">
                    <div class="uk-width-1-4">
                        <span class="uk-text-bold uk-text-uppercase">{{$item->title}}</span> <span class="uk-text-uppercase">{{ $item->name }}</span>
                    </div>
                    <div class="uk-text" style="white-space: pre-line">{{ $item->body }}</div>
                </div>
            </div>
        </div>
    </div>

    @else

    <div class="job_container">
        <div class="job" @if($item->background)style="background-color: {{$item->background}}"@endif>
            <div class="uk-position-relative uk-visible-toggle uk-light">
                <div class="slick_slider">
                    @foreach($item->images as $image)
                    <div class="uk-text-center item">
                        <img src="/storage/images/content_item/{{$image->name}}" alt="" class="uk-display-inline-block">
                    </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="uk-container uk-container-expand">
            <div class="uk-margin-top uk-margin-bottom">
                <div class="uk-grid">
                    <div class="uk-width-1-4">
                        <span class="uk-text-bold uk-text-uppercase">{{$item->title}}</span> <span class="uk-text-uppercase">{{ $item->name }}</span>
                    </div>
                    <div class="uk-width-1-2" style="white-space: pre-line">{{$item->body}}</div>
                    <div class="slideshow-counter">
                        1/6
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
@endforeach


    @include('category_bar')
@endsection

@push('scripts')
   <script src="{{ mix('/js/category.js') }}"></script>
    <script>
      var $status = $('.slideshow-counter');
      var $slickElement = $('.slick_slider');

      $slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
        //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
        var i = (currentSlide ? currentSlide : 0) + 1;
        $status.text(i + '/' + slick.slideCount);
      });

      $slickElement.slick({
        nextArrow: '<div class="uk-position-center-right uk-position-small uk-hidden-hover" href="#"><img src="/images/template/next.png"></div>',
        prevArrow: '<div class="uk-position-center-left uk-position-small uk-hidden-hover" href="#"><img src="/images/template/previous.png"></div>',
        autoplay: true
      });
    </script>
@endpush