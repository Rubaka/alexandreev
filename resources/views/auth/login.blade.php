@extends('template')

@section('content')
    <div class="uk-container uk-flex uk-flex-center">
        <div class="uk-card uk-card-body uk-card-default uk-width-1-4">
            <div class="uk-card-title">{{ __('Login') }}</div>

            <form method="POST" action="{{ route('login') }}">
                @csrf

                <div class="uk-margin">
                    <label for="email" class="uk-label">{{ __('E-Mail Address') }}</label>

                    <input id="email" type="email"
                           class="uk-input{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                           value="{{ old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                    @endif
                </div>

                <div class="uk-margin">
                    <label for="password" class="uk-label">{{ __('Password') }}</label>

                    <input id="password" type="password"
                           class="uk-input{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"
                           value="{{ old('password') }}" required autofocus>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                    @endif
                </div>

                <div class="uk-margin">
                    <label>
                        <input class="uk-checkbox" type="checkbox"
                               name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                    </label>
                </div>

                <div class="uk-margin">
                    <button type="submit" class="uk-button uk-button-default uk-width-1-1">
                        {{ __('Login') }}
                    </button>
                </div>
                <div class="uk-margin">
                    <a class="uk-link" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                </div>
            </form>
        </div>
    </div>
@endsection
