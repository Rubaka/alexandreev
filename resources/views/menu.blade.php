<ul class="uk-navbar-nav">
    @foreach($itemsMenu as $item)
        <li>
            <a href="/category/{{$item->id}}">{{ $item->name }}</a>
            @if($item->children->count())
                <div class="drop_down_menu">
                    <ul class="uk-nav uk-dropdown-nav">
                        @foreach($item->children as $childItem)
                            <li><a href="/category/{{$childItem->id}}">{{$childItem->name}}</a></li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </li>
    @endforeach
</ul>