<li {!! (request()->is(substr($url, 1))) ? 'class="uk-active"' : '' !!}>
    <a href="{{ $url }}">{{ $name }}</a>
</li>