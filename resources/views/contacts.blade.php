@extends('template')

@section('content')
    <div class="uk-container uk-container-expand">
        <div class="uk-grid">
            <div class="uk-width-1-4">
                <div class="contact_left_column">
                    <h2>Hello!</h2>
                    <img src="/storage/images/avatar.jpg?{{ filemtime(storage_path('app/public/images/avatar.jpg')) }}" alt="" class="uk-border-circle avatar">

                    <h2>Contact</h2>
                    <div>
                        - <br>
                        {{ $email }} <br>
                        {{ $phone }}
                    </div>

                    <h2>Follow me</h2>
                    <div>
                        - <br>
                        @foreach($links as $link)
                            <a target="_blank" href="{{ $link['href'] }}">
                                {{ $link['text'] }}
                            </a><br>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="uk-width-3-4">
                <div class="uk-width-2-3">
                    <div>
                        <h2>About</h2>
                        {{ $page->content }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection