<!DOCTYPE html>
<html>
<head>
    <title>Alexandreev</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
</head>
<body>
    <div class="uk-container-expand wrapper">
        <div class="content">
            <header class="uk-text-uppercase" uk-sticky>
                <nav uk-navbar>
                    <div class="uk-navbar-left">
                        <div class="uk-grid uk-grid-collapse uk-flex-middle logo">
                            <div>
                                <a href="/" class="logo_img_link">
                                    <img src="/images/template/logo.svg">
                                </a>
                            </div>
                            <div>
                                <a href="/" class="logo_text_link">ALEXANDRIEV</a>
                            </div>
                        </div>
                    </div>

                    <div class="uk-navbar-center">
                        @include('menu')
                    </div>

                    <div class="uk-navbar-right">
                        <a href="/contacts" class="uk-navbar-item">
                            <span>about/contact</span>
                        </a>
                    </div>
                </nav>
                <div class="uk-navbar-dropbar"></div>
            </header>

            <div>@yield('content')</div>

        </div>

        <footer class="uk-container-expand">
            <div class="uk-grid uk-child-width-1-3 ">
                <div>
                    @if(!Request::is('contacts'))
                        <?php $links = json_decode(\App\Page::byAlias('contacts')->data)->links; ?>

                        <div class="uk-grid footer_link">
                            @foreach($links as $key => $link)
                            <div class="item">
                                @if($key > 0)
                                    <span class="circle"></span>
                                @endif
                                <a href="{{ $link->href }}">{{ $link->text }}</a>
                            </div>
                            @endforeach
                        </div>
                    @endif
                </div>

                <div class="uk-text-center">
                    <a href="#" uk-scroll>
                        <img src="/images/template/to_top.jpg">
                    </a>
                </div>

                <div class="uk-text-right">
                    All rights reserved © Alexandriev 2016
                    @if(Auth::check())
                        <br>
                        <a href="/admin/home-page">Admin</a>
                        <a href="/logout">Logout</a>
                    @endif
                </div>
            </div>
        </footer>
    </div>

    <script src="{{ mix('/js/app.js') }}"></script>
    @stack('scripts')
</body>
</html>