@extends('admin.template', ['title' => 'Home page'])

@section('content')
    <form action="/admin/home-page/save" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="uk-grid uk-margin-bottom">
            <div class="uk-width-1-3">
                <div>
                    <div><label>Image</label></div>
                    @if($image)
                        <img src="{{ $image }}?{{ filemtime(storage_path('app/public/images/index/header.jpg')) }}" class="uk-width-1-1 uk-margin-bottom">
                    @endif
                    <div uk-form-custom="target: true">
                        <input type="file" name="image">
                        <input class="uk-input uk-form-width-medium" type="text" placeholder="Select file" disabled>
                    </div>
                </div>

            </div>
            <div class="uk-width-expand">
                <div>
                    <label>Text</label>
                </div>
                <textarea
                        class="uk-textarea uk-width-1-1"
                        rows="12"
                        name="content"
                >{{old('content', $page->content)}}</textarea>
            </div>
        </div>

        <button class="uk-button uk-button-primary uk-width-1-1 uk-margin-bottom">
            Save
        </button>
    </form>
@endsection