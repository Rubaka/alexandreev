@extends('admin.template', ['title' => 'Category'])

@section('content')
    <div id="category_menu">
        <menu-category :menu="menu"></menu-category>
    </div>
@endsection

@push('scripts')
    <script>
        var categories = '{!! $categories !!}'
    </script>
    <script src="{{ mix('js/admin/category.js') }}"></script>
@endpush