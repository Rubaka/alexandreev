<!DOCTYPE html>
<html>
<head>
    <title>Alexandreev</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ mix('css/admin/app.css') }}">
</head>
<body>
<div class="uk-container-expand wrapper uk-margin-bottom">
    <nav class="uk-navbar-container" uk-navbar>
        <div class="uk-navbar-center">
            <div class="uk-navbar-left">

                <ul class="uk-navbar-nav">
                    @include('menu_item', ['url' => '/admin/home-page', 'name' => 'Home page'])
                    @include('menu_item', ['url' => '/admin/category', 'name' => 'Category'])
                    @include('menu_item', ['url' => '/admin/content-item', 'name' => 'Content'])
                    @include('menu_item', ['url' => '/admin/contacts', 'name' => 'Contacts'])
                </ul>

            </div>
        </div>
    </nav>
</div>

<div class="uk-container">
    @if(Session::has('message'))
        <div class="uk-alert uk-alert-success">
            {{ Session::get('message') }}
        </div>
    @endif

    @if ($errors->any())
        <div class="uk-alert uk-alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div id="content" class="uk-margin-bottom">
        <h4>{{ $title }}</h4>
        <hr>
        @yield('content')
    </div>
</div>

<script>
    const tokenLaravel = '{{ csrf_token() }}';
</script>

<script src="{{ mix('js/admin/app.js') }}"></script>
@stack('scripts')

</body>
</html>