@extends('admin.template', ['title' => 'Create item'])

@section('content')
    <form method="post" action="/admin/content-item">
        {!! csrf_field() !!}
        @include('admin.content_item.form')
        <button type="submit" class="uk-button uk-button-primary uk-width-1-1">
            Create
        </button>
    </form>
@endsection