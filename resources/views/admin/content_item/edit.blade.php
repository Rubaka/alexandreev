@extends('admin.template', ['title' => 'Update item'])

@section('content')
    <form method="post" action="/admin/content-item/{{$item->id}}">
        {!! method_field('PUT') !!}
        {!! csrf_field() !!}
        @include('admin.content_item.form')
        <button type="submit" class="uk-button uk-button-primary uk-width-1-1">
            Update
        </button>
    </form>
@endsection