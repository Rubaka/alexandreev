@extends('admin.template', ['title' => 'Content items'])

@section('content')
    <div class="uk-flex uk-flex-between">
        <div>
            <a href="/admin/content-item/create" class="uk-button uk-button-default">
                Create
            </a>
        </div>
        <div class="uk-margin-bottom">
            <label>
                <div class="uk-grid uk-grid-small">
                    <div>Category:</div>
                    <div>
                        <vue-select
                                :nodes="categories"
                                :name="'category[]'"
                                :selected="selectedCategory"
                                :css-classes="['uk-select']"
                                :change-select="(value) => {this.selected(value)}"
                        >
                        </vue-select>
                    </div>
                </div>
            </label>
        </div>
    </div>


    <table class="uk-table uk-table-divider uk-table-small">
        <tr>
            <th>id</th>
            <th>title</th>
            <th>weight</th>
            <th>category</th>
            <th></th>
        </tr>
        @foreach($items as $item)
            <tr>
                <td>{{$item->id}}</td>
                <td>{{$item->title}}</td>
                <td>{{$item->weight}}</td>
                <td>{{$item->category ? $item->category->name : null}}</td>
                <td>
                    <a href="/admin/content-item/{{$item->id}}/edit" class="uk-button uk-button-default">
                        Edit
                    </a>
                </td>
                <td>
                    <a href="/admin/content-item/{{$item->id}}/delete" class="uk-button uk-button-danger">
                        Delete
                    </a>
                </td>
            </tr>
        @endforeach
    </table>
@endsection

@push('scripts')
    <script>
      var categories = {!! $categories !!}
      var selectedCategory = {!! (request()->has('category_id')) ? (int) request()->get('category_id') : '""' !!}
    </script>
    <script src="{{ mix('js/admin/content_items_index.js') }}"></script>
@endpush