<div class="uk-grid">
    <div class="uk-width-2-3">
        <div class="uk-margin-bottom">
            <label>
                Title:
                <input type="text" class="uk-input" name="title" value="{{old('title', $item->title)}}">
            </label>
        </div>

        <div class="uk-margin-bottom">
            <label>
                Name:
                <input type="text" class="uk-input" name="name" value="{{old('name', $item->name)}}">
            </label>
        </div>

        <div class="uk-margin-bottom">
            <label>
                Background:
                <input type="color" class="uk-input" name="background" value="{{old('name', $item->background)}}">
            </label>
        </div>

        <div class="uk-margin-bottom">
            <label>
                Category:
                <vue-select
                        :nodes="categories"
                        :name="'category[]'"
                        :selected="selectedCategory"
                        :css-classes="['uk-select']"
                >
                </vue-select>
            </label>
        </div>

        <div class="uk-margin-bottom">
            <label>
                Body:
                <textarea class="uk-textarea" rows="10" name="body">{{old('body', $item->body)}}</textarea>
            </label>
        </div>

        <div class="uk-margin-bottom">
            <label>
                Weight:
                <input type="text" class="uk-input" name="weight" value="{{old('weight', $item->weight ? $item->weight : 1)}}">
            </label>
        </div>
    </div>
    <div class="uk-width-1-3">
        Images:
        <draggable
                v-model="images"
                class="items uk-margin-bottom"
        >
            <div class="uk-margin-bottom" v-for="(image, index) in images" v-if="image.action !== 'delete'">
                <div class="uk-position-relative">
                    <img v-bind:src="'/storage/images/' + getFolder(image.action) + '/' + image.name"
                            class="uk-width-1-1">
                    <button type="button"
                            @click="deleteImage(index)"
                            class="uk-button uk-button-small uk-button-danger uk-position-top-right"
                    >
                        <i class="fas fa-trash"></i>
                    </button>
                </div>
            </div>
        </draggable>

        <div uk-form-custom class="uk-width-1-1" v-if="!uploadImageProcess">
            <input type="file" @change="uploadImage($event)">
            <button class="uk-button uk-button-default uk-width-1-1 uk-margin-bottom"
                    type="button"
                    tabindex="-1">
                Upload image
            </button>
        </div>
        <div class="uk-width-1-1 uk-text-center" v-else>
            <div uk-spinner></div>
        </div>
    </div>
</div>

<textarea name="images" v-model="JSON.stringify(images)" class="uk-hidden"></textarea>

@push('scripts')
    <script>
      var categories = {!! $categories !!}
      var selectedCategory = {!! ($item->category_id) ? $item->category_id : '""' !!}
      var images = {!! json_encode($images) !!}
    </script>
    <script src="{{ mix('js/admin/content_items.js') }}"></script>
@endpush