@extends('admin.template', ['title' => 'Home page'])

@section('content')
    <form action="/admin/contacts/save" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="uk-grid uk-margin-bottom">
            <div class="uk-width-1-3">
                <div class="uk-margin-bottom">
                    <div><label>Image</label></div>
                    @if($image)
                        <img src="{{ $image }}?{{ filemtime(storage_path('app/public/images/avatar.jpg')) }}}" class="uk-width-1-1 uk-margin-bottom">
                    @endif
                    <div uk-form-custom="target: true">
                        <input type="file" name="image">
                        <input class="uk-input uk-form-width-medium" type="text" placeholder="Select file" disabled>
                    </div>
                </div>

                <div class="uk-margin-bottom">
                    <label>
                        Phone:
                        <input type="text" name="phone" value="{{ old('phone', $phone) }}" class="uk-input">
                    </label>
                </div>

                <div class="uk-margin-bottom">
                    <label>
                        Email:
                        <input type="text" name="email" value="{{ old('email', $email) }}" class="uk-input">
                    </label>
                </div>

                Links:
                <draggable
                        v-model="links"
                        class="items uk-margin-bottom"
                        :options="{handle:'.item_header'}"
                >
                    <div v-for="(link, index) in links">
                        <div class="uk-flex uk-flex-middle uk-grid uk-grid-small">
                            <div class="item_header" style="cursor: pointer">
                                <i class="fas fa-arrows-alt-v"></i>
                            </div>
                            <div class="uk-width-expand">
                                <div class="uk-margin-small-top"><hr></div>
                                <div>
                                    <label>
                                        Text:
                                        <input type="text" v-model="link.text" class="uk-input">
                                    </label>
                                    <label>
                                        Href:
                                        <input type="text" v-model="link.href" class="uk-input">
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </draggable>

                <button class="uk-button uk-button-default uk-width-1-1" @click="addLink" type="button">
                    Add link
                </button>

                <textarea class="uk-hidden" v-model="JSON.stringify(links)" name="links"></textarea>
            </div>
            <div class="uk-width-expand">
                <div>
                    <label>Text</label>
                </div>
                <textarea
                        class="uk-textarea uk-width-1-1"
                        rows="12"
                        name="content"
                >{{old('content', $page->content)}}</textarea>
            </div>
        </div>

        <button class="uk-button uk-button-primary uk-width-1-1 uk-margin-bottom">
            Save
        </button>
    </form>
@endsection

@push('scripts')
    <script>
      var links = '{!! $links !!}'
    </script>
    <script src="{{ mix('js/admin/contacts.js') }}"></script>
@endpush