<div class="uk-grid uk-grid-collapse uk-child-width-1-4 category_grid" uk-grid>
    @foreach($itemsBar as $item)
        <div class="uk-position-relative category_image">
            <a href="/category/{{$item['id']}}" class="uk-display-block">
                <div class="uk-position-top-left uk-position-z-index uk-text-uppercase title">
                    {{$item['name']}}
                </div>
                <div class="uk-transition-toggle uk-inline-clip uk-position-relative uk-width-1-1">
                    <div class="uk-position-top overlay"></div>
                    <div class="uk-background-cover uk-width-1-1 uk-display-block category_grid_height" uk-img
                         data-src="/storage/images/categories/{{$item['image']}}"
                    >
                    </div>
                </div>
            </a>
        </div>
    @endforeach
</div>

@push('scripts')
    <script>
      function setWeight () {
        let elements = document.getElementsByClassName('category_grid_height')
        Array.prototype.forEach.call(elements, function (el) {
          el.style.height = el.offsetWidth + 'px'
        })
      }

      setWeight()
      window.onresize = function (event) {
        setWeight()
      }
    </script>
@endpush