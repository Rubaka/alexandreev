import UIkit from 'uikit/dist/js/uikit.js'

let dropDownMwnus = document.querySelectorAll('.drop_down_menu')

dropDownMwnus.forEach(function (item) {
  item.style.minWidth = item.parentElement.offsetWidth + 'px'
})

UIkit.dropdown('header .drop_down_menu', {
  pos: 'bottom-center',
  offset: 0,
})
