import VueSelect from 'vue-hierarchical-select'

new Vue({
  el: '#content',
  components: {VueSelect},
  methods: {
    selected: function (value) {
      let categoryParameter = '';

      if (value) {
        categoryParameter = '?category_id=' + value
      }

      window.location = '/admin/content-item' + categoryParameter
    }
  },
  data: function() {
    return {
      categories: categories,
      selectedCategory: selectedCategory
    }
  }
})