import VueSelect from 'vue-hierarchical-select'
import VueResource from 'vue-resource'
import draggable from 'vuedraggable'

Vue.use(VueResource)

new Vue({
  el: '#content',
  methods: {
    uploadImage ($event) {
      let formData = new FormData()
      formData.append('image', $event.target.files[0])
      formData.append('_token', tokenLaravel)
      this.uploadImageProcess = true

      this.$http.post('/admin/content-item/upload-image', formData).then(response => {
        this.images.push({
          action: 'new',
          name: response.data
        })
        this.uploadImageProcess = false
      })
    },
    deleteImage (index) {
      this.images[index].action = 'delete';
    },
    getFolder (action) {
      return (action === 'old') ? 'content_item' : 'tmp'
    }
  },
  components: {VueSelect, draggable},
  data: {
    categories: categories,
    selectedCategory: selectedCategory,
    uploadImageProcess: false,
    images: images
  }
})